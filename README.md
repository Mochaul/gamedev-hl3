# Half-Life 3 Gamedev Gamejam

itch.io account: https://mochaul.itch.io

## Daftar Fitur Wajib
Berikut ini adalah daftar fitur minimal/wajib yang harus diimplementasikan di dalam game
Anda:
1. Ada simulasi physics sebagai bagian dari gameplay (misal: objek pemain atau musuh
memiliki node Rigidbody, ada perhitungan gaya gesek antara dua objek ketika
bersentuhan, dsb.).

2. Ada user interface yang memberikan informasi mengenai state permainan (misal:
nyawa pemain, peta, durasi permainan).

3. Ada menu awal permainan yang memberikan opsi kepada pemain untuk mulai
bermain atau keluar dari permainan.

4. Ada end condition (misal: win/lose, time limit) sebagai outcome dari permainan.

5. Ada minimal 1 level yang playable dari awal hingga akhir.

6. Ada minimal 1 tantangan (challenge) yang harus diselesaikan oleh pemain dengan
cara apapun agar dapat menyelesaikan objective dalam permainan.

7. Ada minimal 1 objek (misal: objek geometri primitif, atau avatar karakter berbentuk
manusia, dsb.) yang dikendalikan oleh pemain sebagai karakter di dalam game.

## Daftar Diversifier
1. Utamakan Keselamatan:	Player hanya punya satu nyawa dalam permainan

2. Understandable have a nice day: Implementasikan sebuah bug yang menjadi fitur

3. Didn't I say to make my life average in my next life?!: Parameter atau nyawa karakter pemain akan berkurang setengahnya ketika respawn

## Pengumpulan
Pengumpulan game dilakukan melalui laman game jam 1-Week Game Jam CSUI 2019 di
itch.io. Unggah binary executable game Anda serta data files yang dibutuhkan (jika ada)
dalam sebuah berkas ZIP dan daftarkan game tersebut sebagai submission terhadap game
jam 1-Week Game Jam CSUI 2019. Di laman deskripsi game Anda, cantumkan informasi
yang menjelaskan game Anda kepada audiens umum, termasuk tautan ke repositori GitLab
public milik Anda yang menyimpan hasil pekerjaan game.
Periode game jam akan dimulai pada tanggal 28 Oktober 2019 pukul 18:00 dan berakhir
pada tanggal 4 November 2019 pukul 21:00.

## Description
Pudge is an alien from planet Kleiner. He accidentally activate a portal from his homeworld and now are stuck on Earth. Let's help him find his way back to his homeworld by traveling all continents of Earth through portals!