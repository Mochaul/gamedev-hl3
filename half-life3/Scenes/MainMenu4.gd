extends LinkButton

export(String) var scene_to_load

func _on_Main_Menu4_pressed():
	global.lives = 1
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
