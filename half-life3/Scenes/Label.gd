extends Label

func _process(delta):
	
	if global.lives < 0.0000009:
		self.text = "Endless mode"
	else:
		self.text = "Live: " + str(global.lives)
