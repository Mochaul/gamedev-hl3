extends Label

func _process(delta):
	
	if global.lives < 0.0000009:
		self.text = "You have helped Pudge\nthrough endless mode"
	else:
		self.text = "You have successfully\nhelped Pudge return to\nhis planets with " + str(global.lives) + "\nlive left!"
